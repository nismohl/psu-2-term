#include <string>
#include <vector>
#include <iostream>

std::vector<int> CalcPrefixFunction(const std::string& s)
{
	std::vector<int> pi(s.length(), 0);

	// n iterations
	// each iteration O(1)
	// n * O(1) -> O(n) // �� ��������.

	// O(n)
	// ������ �������� while -> ���������� �������� k
	// 

	// � ������ 0 ����� (k = 0).
	// �� ��� n �������� �� �������� �������� �� ������ n �����.
	// ���-�� ���, ����� �� ��������� ��� ������ ����� <= n.

	for (int i = 1; i < s.length(); i++) {
		int k = pi[i - 1];
		//while (k > 0) {
		//	if (s[i] == s[k]) {
		//		std::cout << i << ": Success: s[" << i << "] == s[" << k << "]\n";
		//		break;
		//	}
		//	std::cout << i << ": Fail:    s[" << i << "] != s[" << k << "]\n";
		//	k = pi[k - 1];
		//}
		//std::cout << k << "\n";
		while (k > 0 && s[i] != s[k]) {
			k = pi[k - 1];
			//std::cout << k << "\n";
		}
		//pi[i] = k + (s[i] == s[k] ? 1 : 0);

		if (s[i] == s[k]) {
			pi[i] = k + 1;
		}
		else {
			pi[i] = s[i] == s[0] ? 1 : 0;
		}
	}
	return pi;
}


std::vector<int> CalcZFunction(const std::string& s)
{
	std::vector<int> z(s.length(), 0);
	// 
	// [left, right] - z[left] = right - left.
	int left = 0;
	int right = 0;
	for (int i = 1; i < s.length(); i++) {
		if (i > right) {
			while (i + z[i] < s.length()
				&& s[z[i]] == s[i + z[i]]) 
			{
				z[i]++;
			}
			left = i;
			right = i + z[i];
		}
		else {
			const int k = i - left;
			if (z[k] + i < right) {
				z[i] = z[k];
			}
			else {
				z[i] = right - i;
				while (i + z[i] < s.length()
					&& s[z[i]] == s[i + z[i]]) 
				{
					z[i]++;
				}
				left = i;
				right = i + z[i];
			}
		}
	}
	return z;
}

// 1) ����� �������
// pattern # text
// pi, z -> �����

// 2) pi <~> z

// 3) string -> z-function
// ������: �������������� ������.
// z(pi)-function -> string

// ������: �������������� ������.
// z-function -> string
std::string reconstructString(const std::vector<int>& z)
{
	std::string s(z.size(), 'a');
	char maxChar = 'a';
	for (int i = 1; i < z.size(); i++) {
		if (z[i] == 0) {
			s[i] = ++maxChar;
		}
		else {
			// ������������ z[i] ��������� �� ������ ������
			// � ��������� � ������� i
			const int count = z[i];
			const int right = i + count;
			for (int j = 0; j < count; j++, i++) {
				if (i + z[i] > right) {
					break;
				}
				s[i] = s[j];
			}
			i--;
		}
	}
	return s;
}

// 4) ����� ���� ������ s, ���� pattern.
// 4.1) ������: ����� ���-�� ��������� s � pattern (OK).
// 4.2) ������: ����� ���-�� ��������� ������� �������� pattern (?).
//      �� �������� �����.

// pattern = "aba"
// s = "aaaabcaaabaabc"
// 4.1) 2
// 4.2) prefix of 1 length = 9
//      prefix of 2 length = 3
//      prefix of 3 length = 1

// pi[i] = k   ->  � ������� i ������������� ������� ����� k
//             ->  � ������� i ������������� ������� ����� pi[k-1]
//             ->  � ������� i ������������� ������� ����� pi[pi[k-1]-1]
//             ->  ...

std::vector<int> findAllPrefixes(const std::string& pattern,
	const std::string& text)
{
	// TODO: '#' ������ ���� ������ pattern � text
	const std::string s = pattern + "#" + text;
	
	// TODO: �� ������� ��� �������-�������, � ������ �� �����.
	const auto pi = CalcPrefixFunction(s);

	std::vector<int> counts(pattern.size() + 1, 0);
	for (int i = pattern.size() + 1; i < s.size(); i++) {
		counts[pi[i]]++;
	}

	for (int i = pattern.size(); i > 0; i--) {
		counts[pi[i -
			1]] += counts[i];
	}

	return counts;
}

// 5) ����� ������ ������
//    s = t^k  ~  s = tttttttt (k ���)
//    ��� ���� t ����� ����������� ����� ( s = tt tt tt tt)

//    s = t^k -> ��� ����� ������� � �������-������� s?
//    value = s.length() - pi.back();
//       4  =     16     -    12
//    ��������: ���� |s| % value == 0  <->  s ����� ������ value.
//    
//    <-  s = tttttttt, pi.back() = |s| - |t|  ???
//    ->  |s| % value  -> |s| = value * k
//                        value = value * k - pi.back()
//                        pi.back() = value * (k - 1).

// 6) ��� ������ ������� � ������ ����� ������ 
//    ����������� � ������� � ���� �������

void dump(const std::vector<int>& d)
{
	for (int v : d) {
		std::cout << v << " ";
	}
	std::cout << "\n";
}

void dump(const std::string& d)
{
	for (char v : d) {
		std::cout << v << " ";
	}
	std::cout << "\n";
}

int main()
{
	//const std::string s = "abacabadaababacabacabadaba";
	//// s -> z(s) -> s* -> z(s*)
	//// assert(z(s) == z(s*))
	//const auto z = CalcZFunction(s);
	//
	//const std::string sResonstructed = reconstructString(z);
	//const auto zSecond = CalcZFunction(sResonstructed);

	//dump(s);
	//dump(z);
	//std::cout << "\n";
	//dump(sResonstructed);
	//dump(zSecond);

	//std::cout << (z == zSecond) << "\n";

	//const std::string pattern = "aba";
	const std::string s = "abcdabcdabcdabcd";
	dump(s);
	dump(CalcPrefixFunction(s));

	return 0;
}
