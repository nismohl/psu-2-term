#include <vector>
#include <iostream>
#include <utility>
#include <algorithm> 
#include <unordered_set>

typedef std::vector<std::vector<int> > Graph;

class CBridgeFinder {
public:
    std::vector<std::pair<int, int> > find_bridges(const Graph& g);
    std::vector<int> find_cut_points(const Graph& g);
    std::vector<int> topological_sort(const CGraph& g);

private:
    int timer;
    std::vector<int> parents;
    // dp[v] = min time_in[w], где {child, w} - обратное ребро 
    //       выбор минимума time_in по всем окончаниям обратных ребрам у потомков.
    std::vector<int> dp;
    std::vector<int> time_in;
    std::vector<int> time_out;
    std::vector<bool> is_visited;

    void dfs(const Graph& graph, int v);
    std::vector<std::pair<int, int> > select_bridges();
    std::vector<int> select_cut_points();
    void reset(int size);
};

void CBridgeFinder::dfs(const Graph& graph, int v)
{
    //std::cout << v << " " << parents[v] << "\n";
    is_visited[v] = true;
    time_in[v] = timer++;
    dp[v] = time_in[v];

    for(int to : graph[v]) {
        if(to == parents[v]) {
            // ребро в родителя
        } else if(is_visited[to]) {
            // обратное ребро
            if(time_in[to] < dp[v]) {
                std::cout << v << " relaxed (" << dp[v] << " -> " << time_in[to] << ") by backward edge to " << to << "\n";
            }
            dp[v] = std::min(dp[v], time_in[to]);
        } else {
            // ребро в потомка в дереве (ребро дерева)
            parents[to] = v;
            dfs(graph, to);
            if(dp[to] < dp[v]) {
                std::cout << v << " relaxed (" << dp[v] << " -> " << dp[to] << ") by child " << to << "\n";     
            }
            dp[v] = std::min(dp[v], dp[to]);
        }
    }

    std::cout << v << ": " << time_in[v] << " -> " << dp[v] << "\n";
}

std::vector<std::pair<int, int> > CBridgeFinder::select_bridges()
{
    std::vector<std::pair<int, int> > bridges;
    for(int i = 0; i < dp.size(); i++) {
        if(parents[i] != -1
            && dp[i] == time_in[i]) 
        {
            std::pair<int, int> bridge = std::make_pair(parents[i], i);
            bridges.push_back(bridge);
        }
    }
    return bridges;
}

std::vector<int> CBridgeFinder::select_cut_points()
{
    std::unordered_set<int> cut_points;
    // Проверка корня на точку сочленения:
    // есть хотя бы два потомка у корня.
    if(std::count(parents.begin(), parents.end(), 0) >= 2) {
        cut_points.insert(0);
    }
    for(int i = 1; i < dp.size(); i++) {
        // i - потомок parents[i]
        // если dp[i] >= time_in[parents[i]], то 
        //    в поддереве с корнем в i 
        //    нет обратного ребра в предка вершины parents[i]
        if(dp[i] >= time_in[parents[i]]) {
            cut_points.insert(parents[i]);
        }
    }
    return std::vector<int>(cut_points.begin(), cut_points.end());
}

void CBridgeFinder::reset(int size)
{
    timer = 0;
    dp = std::vector<int>(size, -1);
    time_in = std::vector<int>(size, -1);
    time_out = std::vector<int>(size, -1);
    is_visited = std::vector<bool>(size, false);
    parents = std::vector<int>(size, -1);
}

std::vector<int> CBridgeFinder::find_cut_points(const Graph& g)
{
    reset(g.size());
    // WARNING: проверить на связность исходный граф. 

    //for(int from = 0; from < g.size(); from++) {
    dfs(g, 0);
    //}

    return select_cut_points();
}

std::vector<std::pair<int, int> > CBridgeFinder::find_bridges(const Graph& g)
{
    reset(g.size());
    // WARNING: проверить на связность исходный граф. 

    //for(int from = 0; from < g.size(); from++) {
    dfs(g, 0);
    //}

    return select_bridges();
}

void add_edge(Graph& g, int from, int to)
{
    g[from].push_back(to);
    g[to].push_back(from);
}

class CEvenChecker {
public:
    bool operator()(int v) const 
    {return v % 2 == 0;}
};

int main()
{
    //int n, m;
    //std::cin >> n >> m;
    
    Graph graph(9);
    // for(int i = 0; i < m; i++) {
    //     int from, to;
    //     std::cin >> from >> to;
    //     graph[from].push_back(to);
    //     graph[to].push_back(from);
    // }
    add_edge(graph, 0, 1);
    add_edge(graph, 0, 2);
    add_edge(graph, 1, 3);
    add_edge(graph, 2, 3);
    add_edge(graph, 2, 4);
    //add_edge(graph, 3, 4);
    add_edge(graph, 0, 5);
    add_edge(graph, 5, 6);
    add_edge(graph, 5, 7);
    add_edge(graph, 5, 8);
    add_edge(graph, 6, 7);
    add_edge(graph, 7, 8);
    add_edge(graph, 0, 6);

    // std::vector<int> values;
    // values.push_back(0);
    // values.push_back(4);
    // values.push_back(5);
    // values.push_back(6);
    // values.push_back(7);
    // values.push_back(9);
    // values.push_back(11);
    // // 4, 5, 6, 7, 9, 11};
    // std::cout << std::count_if(values.begin(), values.end(), CEvenChecker()) << "\n";

    //const auto bridges = CBridgeFinder().find_bridges(graph);
    // for(const auto& bridge : bridges) {
    //     std::cout << bridge.first << " " << bridge.second << "\n";
    // }

    const auto cut_points = CBridgeFinder().find_cut_points(graph);
    for(const auto& cut_point : cut_points) {
         std::cout << cut_point << "\n";
    }
}

// Конденсация   