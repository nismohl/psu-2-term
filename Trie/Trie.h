#pragma once

#include <map>
#include <string>
#include <vector>
#include <ostream>

// ������� (patterns) - ��, �� ���� ������ ���.
// ����� - ��, � ��� ��� ������� ����� ������.

// 1) ���
// 2) ������� (���-�������)

// 1) ���
// ����� ������ ����� ������.
// * �������� ������� ������� � ����.
// * ������������� ������ (��� �����)
// * ��� ���� ��������:
//   > AddPattern
//   > GetKPattern // �������� k-�� � ��������������� �������.

// 2) ���-�������
// SuffixLink.

// O(|text| + k), k - ���-�� ���������.

class CTrie {
public:
	CTrie();
	~CTrie();

	// � �������� ������������� �������� - id �������.
	int AddPattern(const std::string& pattern);
	bool HasPattern(const std::string& pattern) const;
	// ������ id �������� � ��������������� �������.
	// O(N * L), N - ���-�� �������, L - ����� ������ ��������.
	// O(L * |Sigma|), Sigma - ��-�� ���������� �������� 
	//   (|Sigma| - ������������ ������� �����������).

	// O(NlogN * L) - ����������.
	// O(N * L) - ����������� ����������.
	// O(L * logN) - ����� ����� �������.
	std::vector<int> Sort() const;

	// forward declaration ��� CIterator.
	class CNode;
	class CIterator {
	public:
		// ����������� ��������.
		void Move(char c);
		// �������� �������, ��������������� � ������ ������� (���������).
		void GetPatternsWithEnd(std::vector<int>& result);

		void Reset() { node = trie.root; }

	private:
		// ������� �������-���������.
		CTrie::CNode* node;
		CTrie& trie;

		CIterator(CTrie& trie) : trie(trie), node(trie.root) {}

		friend class CTrie;
	};

	CIterator CreateIterator() { return CIterator(*this); }
	// ���� ��������� ���� ��� ����������� ����������� ����� ���������� graphviz.
	void Dump(std::ostream& stream, CIterator* iterator = nullptr) const;

private:
	struct CNode {
	public:
		// char -> CNode*
		// 0..255
		// 1) std::vector<CNode*> // ���������, ��� ������ ���������������
		// 2) std::array<CNode*, 256> // ������ ����� �������
		// 3) std::map<char, CNode*> // ��������� �� �������� �� �� O(1)
		// 4) std::unordered_map<char, CNode*> // ��� 3-�� ����� ��� 4��
		CNode(CNode* parent, char parentChar) : PatternId(-1), SuffixLink(nullptr), 
			Parent(parent), ParentChar(parentChar) {}
		~CNode();

		std::map<char, CNode*> Children;
		// nullptr - ���������� ������ ��� �� ���������.
		CNode* SuffixLink;
		CNode* Parent;
		int PatternId;
		char ParentChar; // a - �� ������� (������ 12. ����� 26).

		CNode* AddChild(char c);
		CNode* Child(char c) const;
	};

	// ������ ����.
	CNode* root;
	int patternsCount;

	typedef std::map<CNode*, int> CEnumeratedNodes;
	void dumpNode(std::ostream& stream, CNode* node, CNode* activeNode, CEnumeratedNodes& enumeratedNodes) const;
	int getNodeId(CEnumeratedNodes& enumeratedNodes, CNode* node) const;

	void traverseInSortedOrder(CNode* node, std::vector<int>& result) const;

	CNode* findSuffixLink(CNode* node);

	friend class CIterator;
};
