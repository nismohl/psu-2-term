#include "TrieBasedWildcardPatternFinder.h"

#include <cassert>

void CTrieBasedWildcardPatternFinder::SetPattern(const std::string& pattern, char wildcard)
{
	clean();

	patternLength = pattern.size();
	std::vector<std::string> subPatterns;

	splitPattern(pattern, wildcard, subPatterns, shifts);

	//if (pattern == mergePatterns(subPatterns, subPatternsShifts, wildcard, pattern.size())) {
	//	std::cout << "[OK] " << pattern << "\n";
	//}
	//else {
	//	std::cout << "[FAIL] " << pattern << "\n";
	//}

	for (int i = 0; i < subPatterns.size(); i++) {
		uniqueIndex[trie->AddPattern(subPatterns[i])].push_back(i);
	}
}

int CTrieBasedWildcardPatternFinder::SearchPattern(const std::string& text) const
{
	// ���-�� ��������� �����������, ���� ������ �������� � ������ �������.
	std::vector<int> occurences(text.size(), 0);

	CTrie::CIterator it = trie->CreateIterator();
	for (int i = 0; i < text.size(); i++) {
		it.Move(text[i]);
		std::vector<int> uniquePatternsIds;
		it.GetPatternsWithEnd(uniquePatternsIds);

		updateOccurencies(uniquePatternsIds, i, occurences);
	}

	return std::count(occurences.begin(), occurences.begin() + text.length() - patternLength + 1, shifts.size());
}

void CTrieBasedWildcardPatternFinder::updateOccurencies(const std::vector<int>& uniquePatternsIds, int endPos,
	std::vector<int>& occurencies) const
{
	for (int uid : uniquePatternsIds) {
		const std::vector<int>& sourceSubPatternIds = uniqueIndex.at(uid);
		for (int subPatternId : sourceSubPatternIds) {
			const int startPos = endPos - shifts[subPatternId];
			if (startPos >= 0) {
				occurencies[startPos]++;
			}
		}
	}
}

void CTrieBasedWildcardPatternFinder::clean()
{
	shifts.clear();
	uniqueIndex.clear();
	
	delete trie;
	trie = new CTrie();
}

void CTrieBasedWildcardPatternFinder::splitPattern(const std::string& pattern, char wildcard,
	std::vector<std::string>& subPatterns, std::vector<int>& subPatternsShifts) const
{
	// ������ �������� ���������� ��� ?
	int startPos = 0;
	for (int i = 0; i < pattern.size(); i++) {
		if (pattern[i] == wildcard) {
			const int subPatternSize = i - startPos;
			if (subPatternSize > 0) {
				subPatterns.push_back(pattern.substr(startPos, subPatternSize));
				subPatternsShifts.push_back(i - 1);
			}
			startPos = i + 1;
		}
	}
	if (startPos < pattern.size()) {
		subPatterns.push_back(pattern.substr(startPos, pattern.size() - startPos));
		subPatternsShifts.push_back(pattern.size() - 1);
	}

	//for (const auto& subPattern : subPatterns) {
	//	if (subPattern.find(wildcard) != std::string::npos) {
	//		std::cout << "[FAIL]\n";
	//	}
	//}
}

std::string CTrieBasedWildcardPatternFinder::mergePatterns(const std::vector<std::string>& subPatterns,
	const std::vector<int>& subPatternsShifts, char wildcard, int length) const
{
	assert(subPatterns.size() == subPatternsShifts.size());
	std::string result(length, wildcard);
	for (int i = 0; i < subPatterns.size(); i++) {
		std::copy(subPatterns[i].begin(), subPatterns[i].end(), result.begin() + subPatternsShifts[i]);
	}
	return result;
}
