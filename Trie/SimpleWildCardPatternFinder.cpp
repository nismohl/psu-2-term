#include "SimpleWildcardPatternFinder.h"

void CSimpleWildPatternFinder::SetPattern(const std::string& pattern_, char wildcard_)
{
	pattern = pattern_;
	wildcard = wildcard_;
}

int CSimpleWildPatternFinder::SearchPattern(const std::string& text) const
{
	int count = 0;
	for (int i = 0; i <= text.size() - pattern.size(); i++) {
		if (canApplyPattern(text, i)) {
			count++;
		}
	}
	return count;
}

bool CSimpleWildPatternFinder::canApplyPattern(const std::string& text, int startPos) const
{
	for (int i = 0; i < pattern.size(); i++) {
		if (pattern[i] != wildcard
			&& pattern[i] != text[startPos + i])
		{
			return false;
		}
	}
	return true;
}
