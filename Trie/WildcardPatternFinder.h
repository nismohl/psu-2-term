#pragma once

#include <string>

class IWildcardPatternFinder {
public:
	virtual ~IWildcardPatternFinder() = default;

	// ��������� ���������.
	virtual void SetPattern(const std::string& pattern, char wildcard_) = 0;

	// ����� � ������.
	virtual int SearchPattern(const std::string& text) const = 0;

	virtual std::string GetName() const = 0;
};
