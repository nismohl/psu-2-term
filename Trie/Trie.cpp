#include "Trie.h"
#include <cassert>

CTrie::CTrie() :
	root(new CNode(nullptr, 0)),
	patternsCount(0)
{
	root->SuffixLink = root;
}

// std::shared_ptr / std::weak_ptr
// std::unique_ptr
CTrie::~CTrie()
{
	delete root;
}

int CTrie::AddPattern(const std::string& pattern)
{
	CNode* node = root;
	for (int i = 0; i < pattern.size(); i++) {
		CNode* child = node->Child(pattern[i]);
		if (child != nullptr) {
			node = child;
		}
		else {
			node = node->AddChild(pattern[i]);
		}
	}
	if (node->PatternId == -1) {
		node->PatternId = patternsCount++;
	}
	return node->PatternId;
}

bool CTrie::HasPattern(const std::string& pattern) const
{
	CNode* node = root;
	for (int i = 0; i < pattern.size(); i++) {
		CNode* child = node->Child(pattern[i]);
		if (child == nullptr) {
			return false;
		}
	}
	return node->PatternId != -1;
}

// a < ab, b > ab
std::vector<int> CTrie::Sort() const
{
	std::vector<int> result;
	traverseInSortedOrder(root, result);
	return result;
}

void CTrie::traverseInSortedOrder(CNode* node, std::vector<int>& result) const
{
	if (node->PatternId != -1) {
		result.push_back(node->PatternId);
	}
	for (auto it : node->Children) {
		traverseInSortedOrder(it.second, result);
	}
}

void CTrie::Dump(std::ostream& stream, CIterator* iterator) const
{
	CEnumeratedNodes enumeratedNodes;
	dumpNode(stream, root, iterator == nullptr ? nullptr : iterator->node, 
		enumeratedNodes);
}

void CTrie::dumpNode(std::ostream& stream, CNode* node, CNode* activeNode, CEnumeratedNodes& enumeratedNodes) const
{
	const int nodeId = getNodeId(enumeratedNodes, node);
	const bool isActive = activeNode == node;
	stream << "V\t" << nodeId << "\t" << node->PatternId << "\t" << isActive << "\n";
	if (node->SuffixLink != nullptr) {
		stream << "S\t" << nodeId << "\t" << getNodeId(enumeratedNodes, node->SuffixLink) << "\n";
	}
	for (const auto& it : node->Children) {
		stream << "E\t" << nodeId << "\t" << getNodeId(enumeratedNodes, it.second) << "\t" << it.first << "\n";
		dumpNode(stream, it.second, activeNode, enumeratedNodes);
	}
}

int CTrie::getNodeId(CEnumeratedNodes& enumeratedNodes, CNode* node) const
{
	auto it = enumeratedNodes.find(node);
	if (it != enumeratedNodes.end()) {
		return it->second;
	}
	else {
		return enumeratedNodes.insert({ node, enumeratedNodes.size() }).first->second;
	}
}

CTrie::CNode* CTrie::findSuffixLink(CNode* node)
{
	if (node->SuffixLink == nullptr) {
		// ���� ������ ��� �� ���������.
		if (node->Parent == root) {
			node->SuffixLink = root;
		}
		else {
			// f(r) (������ 12, ����� 26).
			CNode* candidate = findSuffixLink(node->Parent);
			while (candidate != root) {
				if (candidate->Child(node->ParentChar) != nullptr) {
					break;
				}
				else {
					candidate = findSuffixLink(candidate);
				}
			}
			node->SuffixLink = candidate->Child(node->ParentChar);
			if (node->SuffixLink == nullptr) {
				node->SuffixLink = root;
			}
		}
	}
	return node->SuffixLink;
}

/////////////////////////////////////////////////////////////////////////////

// ����������� ��������.
void CTrie::CIterator::Move(char c)
{
	CNode* child = node->Child(c);
	if (child != nullptr) {
		node = child;
	} else if (node != trie.root) {
		node = trie.findSuffixLink(node);
		Move(c);
	}
}

// �������� �������, ��������������� � ������ ������� (���������).
void CTrie::CIterator::GetPatternsWithEnd(std::vector<int>& result)
{
	if (node->PatternId != -1) {
		result.push_back(node->PatternId);
	}
	CNode* curNode = node;

	// TODO: �������� ������ ���������� ������.
	while (curNode != trie.root) {
		curNode = trie.findSuffixLink(curNode);
		if (curNode->PatternId != -1) {
			result.push_back(curNode->PatternId);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

CTrie::CNode::~CNode()
{
	for (auto it : Children) {
		delete it.second;
	}
}

CTrie::CNode* CTrie::CNode::AddChild(char c)
{
	assert(Children.find(c) == Children.end());
	return Children.insert({ c, new CNode(this, c) }).first->second;
}

CTrie::CNode* CTrie::CNode::Child(char c) const
{
	auto it = Children.find(c);
	return it != Children.end() ? it->second : nullptr;
}
