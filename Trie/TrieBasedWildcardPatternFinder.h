#pragma once

#include "WildcardPatternFinder.h"
#include "Trie.h"

class CTrieBasedWildcardPatternFinder : public IWildcardPatternFinder {
public:
	CTrieBasedWildcardPatternFinder() : trie(nullptr), patternLength(0) {}

	virtual void SetPattern(const std::string& pattern_, char wildCard_) override;
	virtual int SearchPattern(const std::string& text) const override;

	virtual std::string GetName() const override { return "TrieBased"; };

private:
	CTrie* trie;
	int patternLength;
	std::vector<int> shifts;
	// ������ ������� � ���� -> ������ ������� � subPatterns.
	std::map<int, std::vector<int>> uniqueIndex;

	void clean();

	void splitPattern(const std::string& pattern, char wildcard,
		std::vector<std::string>& subPatterns, std::vector<int>& subPatternsShifts) const;
	std::string mergePatterns(const std::vector<std::string>& subPatterns,
		const std::vector<int>& subPatternsShifts, char wildcard, int length) const;

	void updateOccurencies(const std::vector<int>& uniquePatternsIds, int endPos,
		std::vector<int>& occurencies) const;
};

