#include "Trie.h"
#include <fstream>
#include <iostream>
#include <cassert>
#include <chrono>

#include "SimpleWildcardPatternFinder.h"
#include "TrieBasedWildcardPatternFinder.h"

// ������:
// 1) ���, ��������� ��� � ����������� ��������, ��������� �������-������� ��� pattern. 

// 2) ������ a?b (? - 1 ����������� ������). a??b.
//    ����� acbabb

// pattern = a?bb?ccc?dddd, ��� a,bb,ccc,dddd - ������ ��� ?
// pattern -> {a, bb, ccc, dddd} 
// Success position = i - length(id) + 1 - shift(id).
// ��� shift(id) - �������� ������� id � ������ ���������� �������.

// shift:
// a = 0
// bb = 2
// ccc = 5
// dddd = 9

//   a?bb?ccc?dddd
// XXXaXbbacccdddda
// 0000000000000000
// 0013000100000001

// grep
// www.{A-Za-z0-9-}*.com

// 3) ������ - ��������� �������� ����-�� �������� �� {0,1}*
//    ���� �������, ���� � ��� ���� �����, ��� ���������.
//    ���� ����� ������� v1, ..., vn
//    ������: �� ������ ����������, ���� �� ���������� ����� ������������ ������.
//    ��� ����� ����� L, �������� ���� ����� M >= L : ��� ���� ������������.
//    ���������: ��� � ����������� ��������.
//    
//    ���������: ����� ����� �������, ��� �������� ���� �������� ����� ������������ ������.
//    ����� �������: {01, 00, 11}.
//    0, 1, 10, 1010101010101

// 4) ��-�� �����. 
//    ��� ��������: 
//       1) �������� ����� �����.
//          0 ^ 1 -> 1
//			1 ^ 0 -> 1
//          0 ^ 0 -> 0
//          1 ^ 1 -> 0
//       2) ����� ����� n, ��� ������� m �� ������ ��-�� ����� f: n -> min{XOR(m, n) �� m}. ����� argmin(f).
//          

void testFinders(const std::vector<IWildcardPatternFinder*>& finders, const std::string& text, 
	const std::string& pattern, char wildcard = '?')
{
	//std::vector<std::pair<int, int>> results;
	for (IWildcardPatternFinder* finder : finders) {

		auto start = std::chrono::steady_clock::now();
		
		finder->SetPattern(pattern, wildcard);
		int result = finder->SearchPattern(text);

		auto end = std::chrono::steady_clock::now();
		auto elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		
		//std::cout << finder->GetName() << ": pattern = " << pattern << "; text = " << text << "; result = " << result << "\n";
		std::cout << finder->GetName() << "; result = " << result << "\n";
		std::cout << "elapsed time: " << elapsed_seconds.count() << " ms\n\n";
	}
}

std::string generateWildcardString(int length, double prob)
{
	std::string result(length, 'a');
	for (int i = 0; i < length; i++) {
		result[i] = rand() % ('b' - 'a') + 'a';
	}

	for (int i = 0; i < int(prob * length); i++) {
		result[rand() % length] = '?';
	}
	return result;
}

int main()
{
	const int size = 10000;

	std::string text = generateWildcardString(size, 0.0);
	
	std::vector<std::string> tests;
	tests.push_back(generateWildcardString(size / 10, 0.01));
	
	std::vector<IWildcardPatternFinder*> finders;
	finders.push_back(new CTrieBasedWildcardPatternFinder());
	finders.push_back(new CSimpleWildPatternFinder());
	
	for (const auto& test : tests) {
		testFinders(finders, text, test);
	}

	return 0;
}

