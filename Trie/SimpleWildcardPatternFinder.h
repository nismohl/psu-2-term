#pragma once

#include "WildcardPatternFinder.h"

class CSimpleWildPatternFinder : public IWildcardPatternFinder {
public:
	virtual void SetPattern(const std::string& pattern_, char wildcard_) override;
	virtual int SearchPattern(const std::string& text) const override;
	virtual std::string GetName() const override { return "Simple"; };

private:
	std::string pattern;
	char wildcard;

	bool canApplyPattern(const std::string& text, int startPos) const;
};

