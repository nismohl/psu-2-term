#include "Traverse.h"
#include <vector>
#include <queue>
#include <deque>
#include <cassert>
#include <set>
#include <unordered_set>

static int relaxCounter = 0;

static void dfs(IGraph& graph,  IVisitor& visitor,
    int vertex, std::vector<bool>& isVisited)
{
    visitor.VisitIn(vertex);
    
    isVisited[vertex] = true;    
    std::vector<int> neighbours;
    graph.FindAllAdjacentOut(vertex, neighbours);
    for(int i = 0; i < neighbours.size(); i++) {
        if(!isVisited[neighbours[i]]) {
            dfs(graph, visitor, neighbours[i], isVisited);
        }
    }

    visitor.VisitOut(vertex);
} 

void DFS(IGraph& graph, IVisitor& visitor, int start)
{
    std::vector<bool> isVisited(graph.VerticesCount(), false);
    dfs(graph, visitor, start, isVisited);
}

// Find shortes path from start vertex (given) to every vertex
// O(V+E)
// 1) weight(e) = 1 для все e.
//    distances = {0,1,...,n-1}
// 2) weight(e) in {0, 1} 
//    O(V+E)
// 3) weight(e) in {0,1,2,3,...,k}
//    a. modify graph
//       w(e) = n -> split ребро на n ребер с фиктивными вершинами.
//       O(k(V+E))
//    b. several queues
//       distances = {0,1,...,k(n-1)}
//       подсказка: заведем k(n-1) + 1 очередей 
//       алгоритм:
//         в момент обработки вершины v по ребру u->v, кладем вершины v в очередь
//            с индексом d[u] + w(u, v)
//         вопрос: 1. несколько копий (корректность, асимптотикой) O(E+kV) 
//                 2. нужно ли честно обрабатывать каждую из копий (НЕТ)
void BFS(IGraph& graph, int start)
{
    std::vector<int> distances(graph.VerticesCount(), graph.VerticesCount());
    // k,k,k,k+1,k+1,k+1

    // i,       j->i
    // k, ..., k-2 
    //   -> neighbours
    std::queue<int> verticesInProcess;
    std::vector<int> parents(graph.VerticesCount(), -1);

    verticesInProcess.push(start);
    distances[start] = 0;

    while(!verticesInProcess.empty()) {
        const int v = verticesInProcess.front();
        verticesInProcess.pop();
        
        std::vector<int> neighbours;
        graph.FindAllAdjacentOut(v, neighbours);
        for(int neighbour : neighbours) {
            if(distances[neighbour] == graph.VerticesCount()) {
                verticesInProcess.push(neighbour);
                distances[neighbour] = distances[v] + 1;
                parents[neighbour] = v;
            }
        }
    }
}

// BFS01

void kBFS(IGraph& graph, int k, int start)
{
    // Общая схема:
    // Мн-во вершин = 2 части:
    // 1) вершины обработанные (isProcessed ~ посчитано правильное расстояние)
    // 2) вершины еще не обработанные полностью
    // Алгоритм:
    // 1) взять вершину с минимальным расстоянием из необработанных и завершить обработку.

    // i-ая очередь - вершины, до которых известен путь длины i
    std::vector<std::queue<int>> queues(k * (graph.VerticesCount()-1) + 1);
    
    // была ли вершина обработана
    // обработана ~ вершину достали из очереди (впервые) и "обработали" ее соседей 
    std::vector<bool> isProcessed(graph.VerticesCount(), false);

    // int ~ double (не очень хорошо !!!).
    std::vector<int> distances(graph.VerticesCount(), k * (graph.VerticesCount() - 1) );

    queues[0].push(start);
    distances[start] = 0;

    for(int queue_id = 0; queue_id < queues.size(); queue_id++) {
        while(!queues[queue_id].empty()) {
            const int v = queues[queue_id].front();
            queues[queue_id].pop();
            if(!isProcessed[v]) {
                isProcessed[v] = true;
                
                std::vector<int> neighbours;
                graph.FindAllAdjacentOut(v, neighbours);
                for(int to : neighbours) {
                    if(isProcessed[to]) {
                        continue;
                    } else {
                        // расстояние от s до to через v.
                        // distances[v] == queue_id
                        const int distance = distances[v] + static_cast<int>(graph.Weight(v, to));
                        if(distance < distances[to]) {
                            distances[to] = distance;
                            queues[distance].push(to);
                        }
                    }
                }
            }
        }
    }
}

// Как модифицировать BFS для задачи SPSP s ~> t
// SSSP - стандартный BFS она сильно сложнее чем SPSP

// K^D
// 2*K^(D/2)  ~ sqrt(K^D)

bool relax(std::vector<double>& distances, int from, int to, double weight)
{
    relaxCounter++;
    assert(weight >= 0);
    if(distances[from] + weight < distances[to]) {
        distances[to] = distances[from] + weight;
        return true;
    }
    return false;
}

// Как модифицировать BFS для задачи SPSP s ~> t
// SSSP - стандартный BFS она сильно сложнее чем SPSP

struct Data {
    int vertex;
    double distance;
    bool operator<(const Data& other) const 
    {
        return distance < other.distance
            // Зачем? улучшает воспроизводимость.
            || distance == other.distance && vertex < other.vertex;
    }

    bool operator==(const Data& other) const {
        return vertex == other.vertex;
    }
};

void Dijkstra(IGraph& graph, int start, std::vector<double>& _distances)
{
    std::vector<double> distances(graph.VerticesCount(), std::numeric_limits<double>::max());
    std::set<Data> toBeProcessed;

    toBeProcessed.insert({start, 0.0});
    distances[start] = 0.0;

    while(!toBeProcessed.empty()) {
        const auto top = *toBeProcessed.begin();
        toBeProcessed.erase(toBeProcessed.begin());

        std::vector<int> neighbours;
        graph.FindAllAdjacentOut(top.vertex, neighbours);
        for(int to : neighbours) {
            if(relax(distances, top.vertex, to, graph.Weight(top.vertex, to))) {
                auto it = toBeProcessed.find({to, distances[to]});
                if(it != toBeProcessed.end()) {
                    toBeProcessed.erase(it);
                }
                toBeProcessed.insert({to, distances[to]});
            }           
        }
    }

    _distances = std::move(distances);
}

int BellmanFord(IGraph& graph, int start, std::vector<double>& _distances)
{
    relaxCounter = 0; 

    std::vector<double> distances(graph.VerticesCount(), std::numeric_limits<double>::max());
    distances[start] = 0.0;
    
    std::unordered_set<int> previousRelaxed;
    previousRelaxed.insert(start);

    for(int i = 0; i < graph.VerticesCount() - 1; i++) {
        std::unordered_set<int> newRelaxed;
        for(int j = 0; j < graph.VerticesCount(); j++) {
            if(previousRelaxed.find(j) == previousRelaxed.end()) {
                continue;
            }
            std::vector<int> neighbours;
            graph.FindAllAdjacentOut(j, neighbours);
            for(int to : neighbours) {
                if(relax(distances, j, to, graph.Weight(j, to))) {
                    newRelaxed.insert(to);
                }
            }
        }
        if(newRelaxed.empty()) {
            break;
        } else {
            std::swap(previousRelaxed, newRelaxed);
        }
    }
    _distances = std::move(distances);
    return relaxCounter;
}
