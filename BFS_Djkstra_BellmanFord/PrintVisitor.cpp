#include "PrintVisitor.h"

void CPrintVisitor::VisitIn(int vertex)
{ 
    s << getIndent() << vertex << "(" << std::endl; 
    indent++;
}

void CPrintVisitor::VisitOut(int vertex)
{ 
    indent--;
    s << getIndent() << ")" << vertex << std::endl;
}

std::string CPrintVisitor::getIndent() const
{
    assert(indent >= 0);
    return std::string(indent, ' ');
}