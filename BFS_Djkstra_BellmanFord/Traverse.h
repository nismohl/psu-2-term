#pragma once

#include "IGraph.h"
#include "IVisitor.h"

void DFS(IGraph& graph, IVisitor& visitor, int start = 0);
void BFS(IGraph& graph, int start = 0);

void Dijkstra(IGraph& graph, int start, std::vector<double>& _distances);
int BellmanFord(IGraph& graph, int start, std::vector<double>& _distances);
