#include <iostream>

#include "ListGraph.h"
#include "PrintVisitor.h"
#include "Traverse.h"

int main()
{
    CListGraph graph;

    graph.AddVertices(7);
    graph.AddEdge(1, 6, 14);
    graph.AddEdge(1, 3, 9);
    graph.AddEdge(1, 2, 7);

    graph.AddEdge(2, 3, 10);
    graph.AddEdge(3, 6, 2);
    graph.AddEdge(5, 6, 9);

    graph.AddEdge(5, 4, 6);
    graph.AddEdge(3, 4, 11);
    graph.AddEdge(2, 4, 15);

    std::vector<double> distances;
    Dijkstra(graph, 1, distances);

    std::vector<double> distances2;
    std::cout << BellmanFord(graph, 1, distances2) << " relaxes\n";

    // std::cout << (distances == distances2) << "\n";
    // for(int i = 0; i < distances.size(); i++) {
    //     std::cout << i << " " << distances[i] << " " << distances2[i] << "\n";
    // }

    return 0;
}
