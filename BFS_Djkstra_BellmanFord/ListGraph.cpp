#include "ListGraph.h"

CListGraph::CListGraph(const IGraph& other)
{
    adjLists.clear();
    adjLists.resize(other.VerticesCount());
    for(int i = 0; i < VerticesCount(); i++) {
        other.FindAllAdjacentOut(i, adjLists[i]);
    }
}

void CListGraph::AddVertices( int count )
{
    assert(count > 0);
    adjLists.resize( adjLists.size() + count );
}

void CListGraph::AddEdge(int from, int to, double weight)
{
    checkVertex(from);
    checkVertex(to);

    if(!hasVertexInList(adjLists[from], to)) {
        adjLists[from].push_back(to);
        weights[std::make_pair(from, to)] = weight;
    }

    if(!hasVertexInList(adjLists[to], from)) {
        adjLists[to].push_back(from);
        weights[std::make_pair(to, from)] = weight;
    }
}


void CListGraph::FindAllAdjacentIn(int vertex, 
    std::vector<int>& vertices) const
{
    checkVertex(vertex);
    vertices.clear();
    for(int i = 0; i < adjLists.size(); i++) {
        if(hasVertexInList(adjLists[i], vertex)) {
            vertices.push_back(i);
        }
    }
}

void CListGraph::FindAllAdjacentOut(int vertex, 
    std::vector<int>& vertices) const
{
    checkVertex(vertex);
    vertices = adjLists[vertex];
}

void CListGraph::checkVertex(int i) const
{
    assert(i >= 0 && i < adjLists.size());
}

double CListGraph::Weight(int from, int to) const
{
    const auto edge = std::make_pair(from, to);
    auto it = weights.find(edge);
    if( it != weights.end() ) {
        return it->second;
    } else {
        return 1e9;
        //return std::numeric_limits<double>::max();
    }
}

bool CListGraph::hasVertexInList(const CAdjList& l, int vertex) const
{
    return std::find(l.begin(), l.end(), vertex) != l.end();
}
