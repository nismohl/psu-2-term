#pragma once

#include "IVisitor.h"
#include <ostream>

class CPrintVisitor : public IVisitor {
public:
    explicit CPrintVisitor(std::ostream& s) :
        s(s), indent(0) {} 
    virtual void VisitIn(int vertex) override;
    virtual void VisitOut(int vertex) override;

private:
    std::ostream& s;
    int indent;

    std::string getIndent() const;
};
