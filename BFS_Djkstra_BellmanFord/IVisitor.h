#pragma once

struct IVisitor {
    virtual void VisitIn(int vertex) = 0;
    virtual void VisitOut(int vertex) = 0;
};
