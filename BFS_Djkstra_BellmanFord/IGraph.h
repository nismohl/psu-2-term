#pragma once

#include <vector>

struct IGraph {
    virtual ~IGraph() {}
    // Добавить count вершин в граф.
    virtual void AddVertices( int count ) = 0;
	// Добавление ребра от from к to.
    virtual void AddEdge(int from, int to, double weight) = 0;

	virtual int VerticesCount() const  = 0;

    virtual void FindAllAdjacentIn(int vertex, 
        std::vector<int>& vertices) const = 0;
    virtual void FindAllAdjacentOut(int vertex, 
        std::vector<int>& vertices) const = 0;
    virtual double Weight(int from, int to) const = 0;
};

