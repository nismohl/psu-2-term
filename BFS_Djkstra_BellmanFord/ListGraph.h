#pragma once

#include "IGraph.h"
#include <unordered_map>

struct pairhash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
  }
};

class CListGraph : public IGraph {
public:
    CListGraph() = default;
    CListGraph(const IGraph& other);

    virtual void AddVertices( int count ) override;
    virtual void AddEdge(int from, int to, double weight) override;
	virtual int VerticesCount() const override
        { return adjLists.size(); }

    virtual void FindAllAdjacentIn(int vertex, 
        std::vector<int>& vertices) const override;
    virtual void FindAllAdjacentOut(int vertex, 
        std::vector<int>& vertices) const override;

    virtual double Weight(int from, int to) const override;

private:
    typedef std::vector<int> CAdjList;
    std::vector<CAdjList> adjLists;

    std::unordered_map<std::pair<int, int>, double, pairhash> weights;

    void checkVertex(int i) const;
    bool hasVertexInList(const CAdjList& l, int vertex) const;
};
