/*
������ 6.

S = abcaa
T = aca

abcaa$

$abcaa
a$abca
aa$abc
abcaa$
bcaa$a
caa$ab

// ����� �� ��� ������ �����������.
New_S = S$T#

O((|S|+|T|)*log(|S|+|T|))

abcaa$aca#

�����: a, c, ca

	#abcaa$aca
	$aca#abcaa

					ignore_count � ������ ��������
T	1	a#abcaa$ac	0
S	1	a$aca#abca	1
S	1	aa$aca#abc	1
S	1	abcaa$aca#	1
T	0	aca#abcaa$	1
S	0	bcaa$aca#a	0
T	2	ca#abcaa$a	0
S	0	caa$aca#ab	2

���������� �������� "��������".
1) �������� �������� i, i+1
2) ��������� � ������ ������� S � T
3) lcp[i] > 0
4) UPDATE: ignore_count = lcp[i]

���.1: ���� ��� ������ ��������, �� ���-�� ����� �����, �������� � � S, � � T, ����� lcp[i].

���.2: ���� �� ��������� ������ �������� � ������� j, �� ������� �� lcp[j] ���������, ������������ ��� �����������, �� �� �������� ��������������� �������.

������ ��������:
���.3: ��� ����� ������������ ignore_count = min{ lcp[k] } ��� k �� i �� j
	ignore_count = min(ignore_count, lcp[k])

�����������:
��������� ������� s, � ������� SA = [0, 1, 2, ... , 99], ��� ���� s \in {a-z}+

aaaaaaaaaaaaaaab
aaaaaaaaaaaaaab
aaaaaaaaaaaaab
...
ab
b

*/