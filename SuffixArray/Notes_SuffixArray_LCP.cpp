#include <vector>

// ���������� ������
// �� ������ ����� ������ ������ ����. ������?

// ��������� ����. ������:

/*

O(n*logn) - �������� ��� �� ������.

// abcaa

4 a
3 aa
0 abcaa
1 bcaa
2 caa

*/

/*

S - ������, P - ������.
������: ����� ��������� ������� P � S.

��� ������, ��� ������ P ������ � ������ S (� �������� ���������) ~ ���� ������� Si, ��� �������� P - �������.
O(|P|*log|S|).

O(|P|*|S|)

*/

// LCP = Longest Common Prefix
// function_LCP(s, t) = n: s[0..n-1] == t[0..n-1], s[n] != t[n] (+ ���� �� ����� ����� �����������).
//
// Suffix Array -> lcp.
// lcp[i] = function_LCP(i-�� �������, i+1 ��������), ������ ��� i �� �������� �������, �������� SA.

// O(n) - �������� Kasai lcp.

/*

O(n*logn) - �������� ��� �� ������.

// abcaa

SA	lcp

4	1	a		!
3	1	aa
0	0	abcaa	!
1	0	bcaa
2	0	caa

LCP(������� � ���������� i, ������� � ���������� j) = min{ lcp[k] } ��� k ��������� �� i �� j-1

left, right, mid
LCP_left = LCP(������� � �������� left, pattern)
LCP_right = LCP(������� � �������� right, pattern)

LCP_mid = LCP(������� � �������� left, ������� � �������� mid)

LCP_left ? LCP_mid
1) LCP_left <  LCP_mid
   ���� � ������ ��������.
2) LCP_left == LCP_mid
   ��������� �������������� �������� mid � pattern, ������� � LCP_mid.
3) LCP_left >  LCP_mid
   ���� � ����� ��������.

pattern = aac

l   aabaa
x	aac
m	aabcc
...	
r

��� ���������� �������� ��� ����� � ������ �������?

*/


