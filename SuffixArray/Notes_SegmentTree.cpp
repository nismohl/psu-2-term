// ������ ��������

// 0+1+...+7
// 0+1 + 2+3		4+5	+ 6+7
// 0+1		2+3		4+5		6+7
// a0	a1	a2	a3	a4	a5	a6	a7	... an

// + �� [a1..a4]
// s[i] = +op (s[2*i], s[2*i + 1])

// 1) ����� �������� +op �� ������� [i..j]
// 2) �������� ������� ai

// ����������� ������:
// 1) O(n)
// 2) O(1)

// ������ ��������:
// 1) O(logn)
// 2) O(logn)

// +, 0 + a = a
// *, 1 * a = a
// &, True & a = a
