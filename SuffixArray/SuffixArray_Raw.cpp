#include <vector>
#include <string>
#include <iostream>

void pocket_sort(const std::vector<int>& classes, std::vector<int>& arr)
{
	std::vector<int> counts(*std::max(classes.begin(), classes.end()), 0);
	for (int i = 0; i < classes.size(); ++i)
		++counts[classes[i]];

	for (int i = 1; i < counts.size(); ++i) {
		counts[i] += counts[i - 1]; // ����� �����.
	}

	std::vector<int> new_arr(arr.size());
	for (int i = arr.size() - 1; i >= 0; --i) {
		new_arr[--counts[classes[i]]] = arr[i];
	}
	arr = std::move(new_arr);
}

std::vector<int> build_suffix_array_step(std::vector<int>& arr, std::vector<int>& classes, int k)
{

	// n - ����� ������.
	// 2) �� ���������� i-�� �������� ���� �������� ������������� �� ������ 2^i ��������.
	//    ������� ��������: Ceil(log2(n)).
	//    Note: ���� ������� ������� ��������������� n, �� ��� ������ ������ �����������.

	//    suffix[i] compare suffix[j] �� �������� k. �������� ��������� ��������� �� �������� k - 1.
	//    classes[]

	const int size = (1 << (k - 1));
	std::vector<int> new_classes(classes.size(), 0);
	int current_class = 0;

	new_classes[arr[0]] = current_class;
	// ���������� �� 2*size
	for (int i = 0; i < arr.size() - 1; i++) {
		// ��������� suffix[arr[i]] � suffix[arr[i+1]]
			
		const int left_first = classes[arr[i]];
		const int right_first = classes[arr[i] + size];

		const int left_second = classes[arr[i + 1]];
		const int right_second = classes[arr[i + 1] + size];

		if (left_first != left_second
			|| right_first != right_second)
		{
			new_classes[arr[i + 1]] = ++current_class;
		}
		else {
			new_classes[arr[i + 1]] = current_class;
		}
	}

	classes = new_classes;
	pocket_sort(classes, arr);
}

std::vector<int> build_suffix_array_step(std::string s)
{
	// ��������� ����������� ������ ��� ��������� ����������� ���������.
	// ����� ������ ���� ������������� �� �����.
	s += '\0';

	// ������ ��������������� ����� �������� k: 
	// suffix[i] ~ suffix[j] <-> s[i:i+2^k] == s[j:j+2^k].
	// suffix[i] -> ������� ����� ������.
	// 1) ������������� �������� �� ������� ������� (���������). // 2^0
	std::vector<int> classes = pocket_sort(s);
	std::vector<int> arr; // ���������� ��������� �� ������� ������.

	for (int k = 1; k < log(s.length()) + 1; k++) {
		build_suffix_array_step(arr, classes, k);
	}

	// Clean '\0'-suffix. ???
	return arr;
}

int main()
{
	std::string s = "abababba";
}
