#include <iostream>
#include "ArcGraph.h"
#include "ListGraph.h"
#include "Traverse.h"
#include "PrintVisitor.h"

struct A {
    void Print() { std::cout << "A"; }
    void Print2() { std::cout << "A"; }
};

struct B {
    virtual void Print() { std::cout << "B"; }
    virtual void Print2() { std::cout << "B"; }
};

int main()
{
    CListGraph graph;
    int vCount = 10;
    graph.AddVertices(vCount);
    for(int i = 0; i < vCount; i++) {
        graph.AddEdge(i, (i + 2) % vCount);
        graph.AddEdge(i, (i + 1) % vCount);
    }

    CPrintedVisitor visitor(std::cout);
    
    std::cout << "LISTS:\n";
    DFS(graph, 0, &visitor);

    //graph.Dump(std::cout);

    CArcGraph arcGraph(graph);
    //arcGraph.Dump(std::cout);

    std::cout << "ARC:\n";

    DFS(arcGraph, 0, &visitor);
}
