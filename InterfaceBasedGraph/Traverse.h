#include "IGraph.h"
#include "IVisitor.h"

void DFS(IGraph& graph, int start = 0,
    IVisitor* visitor = nullptr);
void BFS(IGraph& graph, int start = 0);
