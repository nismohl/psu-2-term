#include "PrintVisitor.h"

void CPrintedVisitor::OnEnter(int vertex)
{
    s << getIndent() << vertex << "(" << std::endl;
    indentWidth++;
}

void CPrintedVisitor::OnLeave(int vertex)
{
    indentWidth--;
    s << getIndent() << ")" << vertex << std::endl;
}

std::string CPrintedVisitor::getIndent() const
{
    return std::string(indentWidth, ' ');
}