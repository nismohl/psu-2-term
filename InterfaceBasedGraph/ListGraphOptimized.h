#pragma once

#include "ListGraph.h"

class CListGraphOptimized : public CListGraph {
public:
    // TODO:
    CListGraphOptimized() = default;
    explicit CListGraphOptimized(IGraph& other);

    virtual void AddVertices( int count ) override;
	// Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) override;

    virtual void FindAllAdjacentIn(int vertex, 
        std::vector<int>& vertices) const override;

private:
    // // i : {j1, ..., jk} есть ребро j* -> i
    std::vector<CAdjacencyList> reversedAdjLists;
};