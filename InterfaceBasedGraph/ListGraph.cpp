#include "ListGraph.h"

CListGraph::CListGraph(IGraph& other)
{
    adjLists.clear();
    adjLists.resize(other.VerticesCount());
    for(int i = 0; i < other.VerticesCount(); i++) {
        other.FindAllAdjacentOut(i, adjLists[i]);
    }
}

void CListGraph::AddVertices( int count )
{
    assert(count > 0);
    adjLists.resize(adjLists.size() + count);
}

void CListGraph::AddEdge(int from, int to)
{
    checkVertex(from);
    checkVertex(to);
    // проверка на петли ?

    if(!hasVertexInList(adjLists[from], to)) {
        adjLists[from].push_back(to);
    }
}

int CListGraph::VerticesCount() const
{
    return adjLists.size();
}

void CListGraph::FindAllAdjacentIn(int vertex, 
    std::vector<int>& vertices) const
{
    checkVertex(vertex);
    vertices.clear();
    for(int from = 0; from < adjLists.size(); from++) {
        if(hasVertexInList(adjLists[from], vertex)) {
            vertices.push_back(from);
        }
    }
}

void CListGraph::FindAllAdjacentOut(int vertex, 
    std::vector<int>& vertices) const
{
    checkVertex(vertex);
    vertices = adjLists[vertex];
}

void CListGraph::Dump(std::ostream& s) const
{
    for(int from = 0; from < adjLists.size(); from++) {
        s << from << " -> [ ";
        for(int to : adjLists[from]) {
            s << to << " ";
        }
        s << "]" << std::endl;
    }
} 

void CListGraph::checkVertex(int v) const
{
    assert(v >= 0 && v < adjLists.size());
}

bool CListGraph::hasVertexInList(const CAdjacencyList& l, 
    int vertex) const
{
    return std::find(l.begin(), l.end(), vertex) != l.end();
}
