#include "Traverse.h"

void dfs(IGraph& graph, int vertex, IVisitor* visitor,
    std::vector<bool>& isVisited)
{
    if( visitor != nullptr ) {
        visitor->OnEnter(vertex);
    }

    isVisited[vertex] = true;
    std::vector<int> neighbours;
    graph.FindAllAdjacentOut(vertex, neighbours);
    for(int neighbour : neighbours) {
        if(!isVisited[neighbour]) {
            dfs(graph, neighbour, visitor, isVisited);
        }
    }

    if( visitor != nullptr ) {
        visitor->OnLeave(vertex);
    }
}

void DFS(IGraph& graph, int start, IVisitor* visitor)
{
    assert(start >= 0 && start < graph.VerticesCount());
    std::vector<bool> isVisited(graph.VerticesCount(), false);
    dfs(graph, start, visitor, isVisited);
}

void BFS(IGraph& graph, int start)
{
    std::vector<bool> isVisited(graph.VerticesCount(), false);

    std::vector<int> currentLevel;
    
    currentLevel.push_back(start);
    isVisited[start] = true;

    while(currentLevel.empty()) {
        std::vector<int> newLevel;
        for(int from : currentLevel) {
            std::vector<int> neighbours;
            graph.FindAllAdjacentOut(from, currentLevel);
            for(int to : neighbours) {
                if(!isVisited[to]) {
                    newLevel.push_back(to);
                    isVisited[to] = true;
                }
            }
        }
        currentLevel = std::move(newLevel);
    }   
}