#pragma once

#include "IGraph.h"

class CListGraph : public IGraph {
public:
    // TODO:
    CListGraph() = default;
    explicit CListGraph(IGraph& other);

    // Добавить count вершин в граф.
    virtual void AddVertices( int count ) override;
	// Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) override;

	virtual int VerticesCount() const override final;

    virtual void FindAllAdjacentIn(int vertex, 
        std::vector<int>& vertices) const override;
    virtual void FindAllAdjacentOut(int vertex, 
        std::vector<int>& vertices) const override;

    virtual void Dump(std::ostream& s) const override; 

protected:
    typedef std::vector<int> CAdjacencyList;

    // i : {j1, ..., jk} есть ребро i -> j*
    std::vector<CAdjacencyList> adjLists;
    // // i : {j1, ..., jk} есть ребро j* -> i
    // std::vector<CAdjacencyList> reversedAdjLists;

    void checkVertex(int v) const;
    bool hasVertexInList(const CAdjacencyList& l, int vertex) const;
};
