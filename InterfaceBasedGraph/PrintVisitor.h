#pragma once

#include "IVisitor.h"
#include <ostream>

class CPrintedVisitor : public IVisitor {
public:
    explicit CPrintedVisitor(std::ostream& s) :
        s(s), indentWidth(0) {}

    virtual void OnEnter(int vertex) override;
    virtual void OnLeave(int vertex) override;
    
private:
    std::ostream& s;
    int indentWidth;

    std::string getIndent() const;
};
