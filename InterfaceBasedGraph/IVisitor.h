#pragma once

struct IVisitor {
    virtual ~IVisitor() = default;
    virtual void OnEnter(int vertex) = 0;
    virtual void OnLeave(int vertex) = 0;
};
