#include "TimerVisitor.h"

void CTimerVisitor::OnEnter(int vertex)
{
    times[vertex] = {timer++, 0};
}

void CTimerVisitor::OnLeave(int vertex)
{
    times[vertex].second = timer++;
}
