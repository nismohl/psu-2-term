#pragma once

#include "IGraph.h"

class CArcGraph : public IGraph {
public:
    // TODO:
    CArcGraph() : 
        verticesCount(0) {}
    explicit CArcGraph(IGraph& other);

    // Добавить count вершин в граф.
    virtual void AddVertices( int count ) override;
	// Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) override;

	virtual int VerticesCount() const override;

    virtual void FindAllAdjacentIn(int vertex, 
        std::vector<int>& vertices) const override;
    virtual void FindAllAdjacentOut(int vertex, 
        std::vector<int>& vertices) const override;

    virtual void Dump(std::ostream& s) const override; 

private:
    int verticesCount;
    std::vector<std::pair<int, int> > edges;

    void checkVertex(int v) const;
};