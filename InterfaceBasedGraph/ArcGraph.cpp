#include "ArcGraph.h"

CArcGraph::CArcGraph(IGraph& other)
{
    verticesCount = other.VerticesCount();
    edges.clear();
    for(int i = 0; i < other.VerticesCount(); i++) {
        std::vector<int> neighbours;
        other.FindAllAdjacentOut(i, neighbours);
        for(int to : neighbours) {
            edges.push_back({i, to});
        }
    }
}

void CArcGraph::AddVertices( int count )
{
    assert(count > 0);
    verticesCount += count;
}

void CArcGraph::AddEdge(int from, int to)
{
    assert(std::find(edges.begin(), edges.end(), 
        std::pair<int, int>(from, to)) == edges.end());
    edges.push_back({from, to});
}

int CArcGraph::VerticesCount() const
{
    return verticesCount;
}

void CArcGraph::FindAllAdjacentIn(int vertex, 
    std::vector<int>& vertices) const
{
    vertices.clear();
    for(const auto& edge : edges) {
        if(edge.second == vertex) {
            vertices.push_back(edge.first);
        }
    }
}

void CArcGraph::FindAllAdjacentOut(int vertex, 
    std::vector<int>& vertices) const
{
    vertices.clear();
    for(const auto& edge : edges) {
        if(edge.first == vertex) {
            vertices.push_back(edge.second);
        }
    }
}

void CArcGraph::Dump(std::ostream& s) const
{
    for(const auto& edge : edges) {
        s << edge.first << " -> " << edge.second << std::endl;
    }
} 

void CArcGraph::checkVertex(int v) const
{
    assert(v >= 0 && v < verticesCount);
}
