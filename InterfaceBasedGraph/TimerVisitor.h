#include "IVisitor.h"
#include <map>

class CTimerVisitor : public IVisitor {
public:
    CTimerVisitor() :
        timer(0) {}
    virtual void OnEnter(int vertex) override;
    virtual void OnLeave(int vertex) override;

    void GetTimes(std::vector<int>& timeIn,
        std::vector<int>& timeOut) const;

private:
    int timer;
    std::map<int, std::pair<int, int> > times;
};